/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 16:45:24 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/13 18:09:09 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <math.h>
#include "wolf3d.h"

void				ft_get_wall_heigth(t_ray *ray, t_wall *wall)
{
	wall->heigth = fabs((int)(WIN_WIDTH / ray->len));
	if ((wall->down = (-wall->heigth / 2) + (WIN_WIDTH / 2)) < 0)
		wall->down = 0;
	if ((wall->up = (wall->heigth / 2) + (WIN_WIDTH / 2)) >= WIN_WIDTH)
		wall->up = WIN_WIDTH - 1;
}

static int			ft_color_get(int r, int g, int b)
{
	int				color;

	color = (0 << 24) + (r << 16) + (g << 8) + (b);
	return (color);
}

static void			ft_draw_img(t_env *e, int x, int y, int color)
{
	unsigned int	col;
	int				*cpy;

	cpy = (int *)e->img.pxl;
	col = mlx_get_color_value(e->mlx, color);
	cpy[y * (e->img.ln / 4) + x] = col;
}

void				ft_draw_col(int x, int up, int down, t_env *e)
{
	int				y;
	int				color;

	y = -1;
	while (++y < WIN_WIDTH)
	{
		if (y > up)
			color = ft_color_get(80, 80, 80);
		else if (y < down)
			color = ft_color_get(160, 160, 160);
		else
		{
			if (e->ray.side && e->ray.dir.y > 0)
				color = ft_color_get(0, 190, 0);
			else if (e->ray.side && e->ray.dir.y <= 0)
				color = ft_color_get(190, 0, 0);
			else if (!e->ray.side && e->ray.dir.x > 0)
				color = ft_color_get(0, 75, 190);
			else
				color = ft_color_get(155, 192, 0);
		}
		ft_draw_img(e, x, y, color);
	}
}

void				ft_draw_all(t_env *e)
{
	t_ray			ray;
	t_map			map;
	t_wall			wall;
	int				i;

	i = -1;
	while (++i < WIN_LEN)
	{
		ray.hit = 0;
		ft_get_ray(&ray, &(e->cam), i);
		ft_get_ray_step(&ray, &map);
		ft_get_ray_hit(&ray, &map, e);
		ft_get_ray_length(&ray, &map);
		e->ray = ray;
		ft_get_wall_heigth(&ray, &wall);
		ft_draw_col(i, wall.up, wall.down, e);
	}
	e->ray.x = ray.x;
	e->ray.y = ray.y;
	e->ray.dir.y = ray.dir.y;
	e->ray.dir.x = ray.dir.x;
	e->ray.side = ray.side;
	mlx_put_image_to_window(e->mlx, e->win, e->img.ptr, 0, 0);
}
