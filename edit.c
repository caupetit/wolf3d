/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/13 15:59:35 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/13 20:16:03 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include "libft.h"

void		ft_put_lab(t_env *e)
{
	int		i;
	int		j;

	i = -1;
	j = -1;
	while (++i < e->row)
	{
		while (++j < e->col)
			ft_printf("%d ", e->grid[i][j]);
		ft_putchar('\n');
		j = -1;
	}
}

static void	ft_edit_block(t_env *e, int dx, int dy)
{
	int		x;
	int		y;

	x = (int)e->ray.x;
	y = (int)e->ray.y;
	if (y + dy > e->col || y + dy < 0 || x + dx > e->row - 1 || x + dx < 0)
		return ;
	if (e->grid[x + dx][y + dy] == 1)
		e->grid[x + dx][y + dy] = 0;
	else if (e->grid[x + dx][y + dy] == 0)
		e->grid[x + dx][y + dy] = 1;
}

void		ft_edit(t_env *e)
{
	if (e->ray.stepx > 0 && e->ray.stepy > 0)
		ft_edit_block(e, 0, 2);
	if (e->ray.stepx < 0 && e->ray.stepy < 0)
		ft_edit_block(e, 0, -2);
	if (e->ray.stepx > 0 && e->ray.stepy < 0)
		ft_edit_block(e, 2, 0);
	if (e->ray.stepx < 0 && e->ray.stepy > 0)
		ft_edit_block(e, -2, 0);
	ft_put_lab(e);
}
