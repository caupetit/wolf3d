# README #

Wolf3D is a first person 3D game using raycasting, a rendering algorythm that draws each column of pixel depending if walls are detected in view-plane.

In this game,map is first empty then you can build your own labyrinth placing blocs anywhere you want.

* * *

# How to use ? #

Use followings commands:

* make

* ./wolf3d [width] [height]
(default value to 10)

Press space to place/remove blocks

* * * 

# Screenshots #

Empty map

![Wolf1.jpg](https://bitbucket.org/repo/nGp6p9/images/2439111437-Wolf1.jpg)

With few blocks

![Wolf2.jpg](https://bitbucket.org/repo/nGp6p9/images/4173988791-Wolf2.jpg)