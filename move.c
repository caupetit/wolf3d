/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 17:04:11 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/11 18:10:53 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "wolf3d.h"

void		ft_move_up(t_env *e)
{
	if (e->grid[(int)(e->cam.x + (e->cam.dir.x * SPEED))][(int)e->cam.y] == 0)
		e->cam.x += e->cam.dir.x * SPEED;
	if (e->grid[(int)e->cam.x][(int)(e->cam.y + (e->cam.dir.y * SPEED))] == 0)
		e->cam.y += e->cam.dir.y * SPEED;
}

void		ft_move_down(t_env *e)
{
	if (e->grid[(int)(e->cam.x - (e->cam.dir.x * SPEED))][(int)e->cam.y] == 0)
		e->cam.x -= e->cam.dir.x * SPEED;
	if (e->grid[(int)e->cam.x][(int)(e->cam.y - (e->cam.dir.y * SPEED))] == 0)
		e->cam.y -= e->cam.dir.y * SPEED;
}

void		ft_rotate(t_env *e, int dir)
{
	double	xdir;
	double	xplan;
	double	rot;

	rot = ROT;
	if (dir)
		rot = -rot;
	xdir = e->cam.dir.x;
	xplan = e->cam.plan.x;
	e->cam.dir.x = (e->cam.dir.x * cos(rot)) - (e->cam.dir.y * sin(rot));
	e->cam.dir.y = (xdir * sin(rot)) + (e->cam.dir.y * cos(rot));
	e->cam.plan.x = (e->cam.plan.x * cos(rot)) - (e->cam.plan.y * sin(rot));
	e->cam.plan.y = (xplan * sin(rot)) + (e->cam.plan.y * cos(rot));
}
