/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_lab.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 16:36:27 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/13 12:23:57 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "wolf3d.h"

static int	*ft_getgrid_line(t_env *e)
{
	int		*line;
	int		i;

	i = -1;
	if (!(line = (int *)malloc(e->col * sizeof(int))))
		exit(0);
	while (++i < e->col)
		line[i] = 0;
	return (line);
}

static void	ft_get_walls(t_env *e)
{
	int		i;
	int		j;

	i = -1;
	j = -1;
	while (++i < e->row)
	{
		while (++j < e->col)
		{
			if (i == 0 || i == e->row - 1 ||  j == 0 || j == e->col - 1)
				e->grid[i][j] = 2;
		}
		j = -1;
	}
}

int			**ft_get_lab(t_env *e)
{
	int		**grid;
	int		i;

	i = -1;
	if (!(grid = (int **)malloc(e->row * sizeof(int *))))
		exit(0);
	while (++i < e->row)
		grid[i] = ft_getgrid_line(e);
	e->grid = grid;
	ft_get_walls(e);
	return (grid);
}

void		ft_del_lab(t_env *e)
{
	int		i;

	i = -1;
	while (++i < e->row)
		free(e->grid[i]);
	free(e->grid);
}
