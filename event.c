/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/13 18:13:21 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/13 18:17:22 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "wolf3d.h"

int			ft_expose_hook(void *e)
{
	ft_draw_all((t_env *)e);
	return (0);
}

int			ft_key_press(int keycode, t_env *e)
{
	if (keycode == UP)
		e->move.up = 1;
	if (keycode == DOWN)
		e->move.down = 1;
	if (keycode == LEFT)
		e->move.left = 1;
	if (keycode == RIGHT)
		e->move.right = 1;
	return (0);
}

int			ft_key_release(int keycode, t_env *e)
{
	if (keycode == ESCAPE)
	{
		ft_del_lab(e);
		exit(0);
	}
	if (keycode == 32)
		ft_edit(e);
	if (keycode == UP)
		e->move.up = 0;
	if (keycode == DOWN)
		e->move.down = 0;
	if (keycode == LEFT)
		e->move.left = 0;
	if (keycode == RIGHT)
		e->move.right = 0;
	return (0);
}

int			ft_loop(t_env *e)
{
	if (e->move.up)
		ft_move_up(e);
	if (e->move.down && !(e->move.up))
		ft_move_down(e);
	if (e->move.left)
		ft_rotate(e, 1);
	if (e->move.right)
		ft_rotate(e, 0);
	ft_draw_all(e);
	return (0);
}
