#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/30 15:52:06 by caupetit          #+#    #+#              #
#    Updated: 2014/01/13 18:15:25 by caupetit         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

.PHONY: clean fclean all re

NAME = wolf3d
SRC = main.c \
	init.c \
	event.c \
	ray.c \
	draw.c \
	move.c \
	get_lab.c \
	edit.c
OBJ = $(SRC:.c=.o)
FLAGS = -Wall -Wextra -Werror -g
LIBSMLX = -lmlx -lXext -lX11
LIBPATHMLX = -L/opt/X11/lib/
CC = cc

all: lib $(NAME)

$(NAME): $(OBJ)
	@$(CC) $(FLAGS) $^ -o $@ -L./libft/ -lft $(LIBSMLX) $(LIBPATHMLX)
	@echo "==> Program [$@] compiled"

%.o: %.c
	@$(CC) $(FLAGS) -c $< -I libft/includes
	@echo "==> [$<] compiled"

lib:
	@make -C libft all

clean:
	@rm -f $(OBJ)
	@make -C libft clean
	@echo "==> Objects removed"
fclean: clean
	@rm -f $(NAME)
	@make -C libft fclean
	@echo "==> Program [$(NAME)] removed"

re: fclean all
