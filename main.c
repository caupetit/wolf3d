/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 15:37:56 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/13 18:24:28 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "/opt/X11/include/X11/X.h"
#include <mlx.h>
#include "wolf3d.h"

int			main(int argc, char **argv)
{
	t_env	e;

	if (argc != 3 && argc != 1)
		ft_exit_warg();
	ft_init_env(&e, argv, argc);
	ft_put_lab(&e);
	e.img = ft_init_img(&e);
	mlx_expose_hook(e.win, ft_expose_hook, &e);
	mlx_hook(e.win, KeyPress, KeyPressMask, &ft_key_press, &e);
	mlx_hook(e.win, KeyRelease, KeyReleaseMask, &ft_key_release, &e);
	mlx_loop_hook(e.mlx, &ft_loop, &e);
	mlx_loop(e.mlx);
	ft_del_lab(&e);
	return (0);
}
