/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 19:00:13 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/13 18:16:01 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <mlx.h>
#include "wolf3d.h"
#include "libft.h"

void		ft_exit_warg(void)
{
	ft_printf("Wolf3d: wrong argument format\n");
	ft_printf("Usage: wolf3d width len\n");
	ft_printf("width and len must be integer between 10 and 100\n");
	ft_printf("Press Space ==> Enjoy !\n");
	exit(0);
}

void		ft_init_cam(t_cam *cam, t_env *e)
{
	cam->height = WIN_WIDTH / 2;
	(void)e;
	cam->dir.x = -1;
	cam->dir.y = 0;
	cam->plan.x = 0;
	cam->plan.y = 0.66;
}

static void	ft_init_sizes(t_env *e, char **argv, int argc)
{
	if (argc == 1)
	{
		e->col = 10;
		e->row = 10;
	}
	else
	{
		if (ft_strlen(argv[1]) > 3 || ft_strlen(argv[2]) > 3)
			ft_exit_warg();
		e->row = ft_atoi(argv[1]);
		if (e->row < 10 || e->row > 100)
			ft_exit_warg();
		e->col = ft_atoi(argv[2]);
		if (e->col < 10 || e->col > 100)
			ft_exit_warg();
	}
	e->cam.x = e->row / 2;
	e->cam.y = e->col / 2;
}

void		ft_init_env(t_env *e, char **argv, int argc)
{
	ft_init_sizes(e, argv, argc);
	e->grid = ft_get_lab(e);
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, WIN_LEN, WIN_WIDTH, "Wolf3D");
	ft_init_cam(&(e->cam), e);
}

t_img		ft_init_img(t_env *e)
{
	t_img	img;

	img.ptr = mlx_new_image(e->mlx, WIN_LEN, WIN_WIDTH);
	img.pxl = mlx_get_data_addr(img.ptr, &(img.bpp), &(img.ln), &(img.nd));
	return (img);
}
