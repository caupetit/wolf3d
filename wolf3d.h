/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/07 15:50:25 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/13 18:43:28 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# define WIN_LEN 1024
# define WIN_WIDTH 768
# define START_X 5
# define START_Y 5

# define ROT 0.05
# define SPEED 0.12

# define ESCAPE 65307
# define UP 65362
# define DOWN 65364
# define LEFT 65363
# define RIGHT 65361
# define SPACE 32

typedef struct	s_vect
{
	double	x;
	double	y;
}				t_vect;

typedef struct	s_cam
{
	double	height;
	double	x;
	double	y;
	t_vect	dir;
	t_vect	plan;
}				t_cam;

typedef struct	s_ray
{
	double	cam_x;
	double	len;
	double	x;
	double	y;
	t_vect	dir;
	int		stepx;
	int		stepy;
	int		hit;
	int		side;
}				t_ray;

typedef struct	s_map
{
	int		x;
	int		y;
	t_vect	distmap;
	t_vect	distnext;
}				t_map;

typedef struct	s_wall
{
	int		heigth;
	int		up;
	int		down;
}				t_wall;

typedef struct	s_img
{
	void	*ptr;
	char	*pxl;
	int		bpp;
	int		ln;
	int		nd;
}				t_img;

typedef struct	s_move
{
	int		up;
	int		down;
	int		left;
	int		right;
}				t_move;

typedef struct	s_env
{
	void	*mlx;
	void	*win;
	t_img	img;
	int		**grid;
	t_cam	cam;
	t_ray	ray;
	t_move	move;
	int		row;
	int		col;
}				t_env;

void		ft_put_lab(t_env *e);

/*
**	get_lab.c
*/
int			**ft_get_lab(t_env *e);
void		ft_del_lab(t_env *e);

/*
**	init.c
*/
void		ft_exit_warg(void);
void		ft_init_cam(t_cam *cam, t_env *e);
void		ft_init_env(t_env *e, char **argv, int argc);
t_img		ft_init_img(t_env *e);

/*
**	event.c
*/
int			ft_loop(t_env *e);
int			ft_key_release(int keycode, t_env *e);
int			ft_key_press(int keycode, t_env *e);
int			ft_expose_hook(void *e);

/*
**	ray.c
*/
void		ft_get_ray(t_ray *ray, t_cam *cam, int current_ray);
void		ft_get_distmap(t_ray *ray, t_map *map);
void		ft_get_ray_step(t_ray *ray, t_map *map);
void		ft_get_ray_hit(t_ray *ray, t_map *map, t_env *e);
void		ft_get_ray_length(t_ray *ray, t_map *map);

/*
**	draw.c
*/
void		ft_get_wall_heigth(t_ray *ray, t_wall *wall);
void		ft_draw_col(int x, int up, int down, t_env *e);
void		ft_draw_all(t_env *e);

/*
**	move.c
*/
void		ft_rotate(t_env *e, int dir);
void		ft_move_up(t_env *e);
void		ft_move_down(t_env *e);

/*
**	edit.c
*/
void		ft_put_lab(t_env *e);
void		ft_edit(t_env *e);

#endif /* !WOLF3D_h */
