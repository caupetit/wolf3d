/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 16:32:32 by caupetit          #+#    #+#             */
/*   Updated: 2014/01/10 18:11:04 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "wolf3d.h"

/*
**	find direction of current ray
*/
void		ft_get_ray(t_ray *ray, t_cam *cam, int current_ray)
{
	double	w;

	w = WIN_LEN;
	ray->cam_x = 2.0 * (double)current_ray / w - 1;
	ray->x = (double)cam->x;
	ray->y = (double)cam->y;
	ray->dir.x = cam->dir.x + cam->plan.x * ray->cam_x;
	ray->dir.y = cam->dir.y + cam->plan.y * ray->cam_x;
}

/*
**	find the length of com pos to next steps of the map
*/
void		ft_get_distmap(t_ray *ray, t_map *map)
{
	if (ray->dir.x < 0)
	{
		ray->stepx = -1;
		map->distmap.x = (ray->x - (double)map->x) * map->distnext.x;
	}
	else
	{
		ray->stepx = 1;
		map->distmap.x = ((double)map->x + 1.0 - ray->x) * map->distnext.x;
	}
	if (ray->dir.y < 0)
	{
		ray->stepy = -1;
		map->distmap.y = (ray->y - (double)map->y) * map->distnext.y;
	}
	else
	{
		ray->stepy = 1;
		map->distmap.y = ((double)map->y + 1.0 - ray->y) * map->distnext.y;
	}
}

void		ft_get_ray_step(t_ray *ray, t_map *map)
{
	double	raydir_xx;
	double	raydir_yy;

	map->x = (int)ray->x;
	map->y = (int)ray->y;
	raydir_xx = ray->dir.x * ray->dir.x;
	raydir_yy = ray->dir.y * ray->dir.y;
	map->distnext.x = sqrt(1.0 + raydir_yy / raydir_xx);
	map->distnext.y = sqrt(1.0 + raydir_xx / raydir_yy);
	ft_get_distmap(ray, map);
}

/*
**	Find where int the map the ray hit
*/
void		ft_get_ray_hit(t_ray *ray, t_map *map, t_env *e)
{
	while (ray->hit == 0)
	{
		if (map->distmap.x < map->distmap.y)
		{
			map->distmap.x += map->distnext.x;
			map->x += ray->stepx;
			ray->side = 0;
		}
		else
		{
			map->distmap.y += map->distnext.y;
			map->y += ray->stepy;
			ray->side = 1;
		}
		if (e->grid[map->x][map->y] > 0)
			ray->hit = 1;
	}
}

/*
**	find ray length from cam pos to wall
*/
void		ft_get_ray_length(t_ray *ray, t_map *map)
{
	double	ret;

	if (ray->side == 0)
	{
		ret = fabs((map->x - ray->x + ((1 - ray->stepx) / 2)) / ray->dir.x);
		ray->len = ret;
	}
	else
	{
		ret = fabs((map->y - ray->y + ((1 - ray->stepy) / 2)) / ray->dir.y);
		ray->len = ret;
	}
}
